import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespuestaTopHeadlines } from '../interfaces/interfaces';
import { environment } from '../../environments/environment';
const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-Key': apiKey
});


@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  
  constructor( private http: HttpClient) { }

  private ejecutarQuery<T>(query:string){
    query = apiUrl + query;
    return this.http.get<T>( query, { headers} );
  }

  getTopHeadLines(){
    //return this.http.get<RespuestaTopHeadlines>(`https://newsapi.org/v2/everything?q=tesla&from=2021-03-06&sortBy=publishedAt&apiKey=711f70e4df1843c2a2697259a8e85301`);
    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=us`);
  }

  getTopHeadLinesCategory(categoria: string){
    return this.ejecutarQuery<RespuestaTopHeadlines>(`/top-headlines?country=us&category=${categoria}`);
  }
}
