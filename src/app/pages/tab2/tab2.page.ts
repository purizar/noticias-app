import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { NoticiasService } from '../../services/noticias.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit  {

  @ViewChild(IonSegment, {static:true}) segment: IonSegment;

  categorias = ['business', 'entertainment','general', 'health',
                'science', 'sports', 'technology'];
  noticias: Article[]=[];

  constructor(private noticiasService: NoticiasService) {}

  ngOnInit(){
    this.segment.value=this.categorias[0];
    this.cargarNoticias(this.categorias[0]);
  }

  cambioCategoria( event ){
      console.log(event.detail.value);
      this.noticias=[];
      this.cargarNoticias(event.detail.value);
  }

  cargarNoticias(categoria: string){

    this.noticiasService.getTopHeadLinesCategory(categoria).subscribe(
      resp=> {
        this.noticias.push(...resp.articles);
      }
    )
  }

}
