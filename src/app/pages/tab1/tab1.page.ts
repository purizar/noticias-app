import { Component } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { NoticiasService } from '../../services/noticias.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  noticias: Article[]=[];
  constructor(private noticiasServices : NoticiasService) {}

  ngOnInit(){
    this.noticiasServices.getTopHeadLines().subscribe(
      resp=> {
        this.noticias.push(...resp.articles);
      }
    )
  }

}
